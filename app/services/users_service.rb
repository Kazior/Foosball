class UsersService

	CRITERIA = [:win_count, :ratio, :perfect_count, :points, :surprise_count]

	def position_in_ranking(user, criteria)
		scores_in_ranking(criteria).index(send(criteria, user))
	end

	def ranking(criteria)
		return unless available?(criteria)

		ranking_users = Array.new(scores_in_ranking(criteria).size){ [] }

		users.each do |user|
			place = position_in_ranking(user, criteria)
			ranking_users[place] << user
		end

		ranking_users
	end

	def win_count(user)
		user.matches.where(winner_id: user.id).size || 0
	end

	def lose_count(user)
		user.matches.where(loser_id: user.id).size || 0
	end

	def points(user)
		win_count(user) - lose_count(user) + perfect_count(user) + (2 * surprise_count(user))
	end

	def perfect_count(user)
		user.matches.where(winner_id: user.id, score: 0).size || 0
	end

	def surprise_count(user)
		user.matches.where(winner_id: user.id, surprise_victory: true).size || 0
	end

	def perfect_player?(user)
		perfect_count(user) > 0
	end

	def ratio(user)
		fail if user.matches.empty?

		win = win_count(user).to_f
		lose = lose_count(user).to_f
		(win / (win + lose)).round(2)

	rescue
		0.0
	end

	def surprise_victory?(winner_id, loser_id)
		win_count(users.find(winner_id)) < win_count(users.find(loser_id))
	end

	private

	def scores_in_ranking(criteria)
		return unless available?(criteria)

		users.each.map { |user| send(criteria, user) }.compact.uniq.sort.reverse
	end

	def users
		@users ||= User.all
	end

	def available?(criteria)
		CRITERIA.include?(criteria)
	end
end
