class MatchValidator < ActiveModel::Validator

  def validate(match)

    if match.winner_id == match.loser_id
    	match.errors[:base] << "Match can't be played by one player with himself"
    end

    if !match.played_at.nil? && ( match.played_at > Time.current )
    	match.errors[:base] << "Match can't be played in the future"
    end
  end
end