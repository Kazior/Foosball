class MatchesController < ApplicationController

  before_action :authenticate_user!, only: [:new, :edit, :update, :destroy, :create]
  before_action :redirect, only: [:new, :create]
  
  expose(:match)

	def index
    @matches = Match.all
  end

  def new
    @match = Match.new(played_at: Time.now.localtime)
  end

  def create
    @match = Match.new(match_params)

    @match.surprise_victory = surprise?(@match) if @match != nil

    if @match.save
      join_match_with_users(@match)
      redirect_to root_path, notice: 'match was successfully created.'
    else
      render action: 'new'
    end
  end

  private

  def match_params
    params.require(:match).permit(:winner_id, :loser_id, :score, :played_at)
  end

  def redirect
    redirect_to(new_user_session_path) unless user_signed_in? && current_user.admin
  end

  def join_match_with_users(match)
    match.users << User.where(id: [match.winner_id, match.loser_id])
  end

  def surprise?(match)
    UsersService.new.surprise_victory?(@match.winner_id, @match.loser_id)
  end
end
