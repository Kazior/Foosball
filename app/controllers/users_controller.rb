class UsersController < ApplicationController

	before_action :users_service

  expose(:user)
  expose(:users)

  def index
    @users = User.all
  end

  def show
    @user = current_user

    respond_to do |format|
      format.html
      format.xml { render :xml => @user }
    end
  end

  private

  def users_service
  	@users_service = UsersService.new
  end
end
