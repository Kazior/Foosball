module UsersHelper

	def fullname(user)
		"#{user.firstname} #{user.lastname}"
	end

	def user_matches_balance(user)
		"win: #{@users_service.win_count(user)},
		lost: #{@users_service.lose_count(user)}"
	end
end
