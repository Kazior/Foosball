module MatchesHelper

	def winner_avatar_url(match, size)
		winner(match).avatar.url(size)
	end

	def loser_avatar_url(match, size)
		loser(match).avatar.url(size)
	end

	def winner(match)
		match.users.find(match.winner_id)
	end

	def loser(match)
		match.users.find(match.loser_id)
	end

	def result(match)
		"10 : #{match.score}"
	end

	def date(match)
		match.played_at.strftime("%d/%m/%Y")
	end

	def time(match)
		match.played_at.strftime("%H:%M")
	end

	def winner_fullname(match)
		fullname(winner(match))
	end

	def loser_fullname(match)
		fullname(loser(match))
	end
end
