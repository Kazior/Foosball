class Match < ActiveRecord::Base

	validates_presence_of :winner_id, :loser_id, :score, :played_at

	validates_with MatchValidator
	
	has_and_belongs_to_many :users

end