# This repesents player......
class User < ActiveRecord::Base

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :matches

	validates_presence_of :firstname, :lastname, :email
	validates_uniqueness_of :email

  has_attached_file :avatar, styles: { medium: "100x100>", thumb: "25x25>" },
  	default_url: "/images/:style/missing.png"

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

end
