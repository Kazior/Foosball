class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.timestamps null: false
      
      t.integer :winner_id
      t.integer :loser_id
      t.integer :score
      t.datetime :played_at
    end
  end
end
