class AddSurpriseVictoryToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :surprise_victory, :boolean, :default => false
  end
end
