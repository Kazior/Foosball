# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(
	firstname: 'Antek',
	lastname: 'Papuga',
	email: 'papuga@mail.com', 
	password: '12345678', 
	password_confirmation: '12345678',
)

User.create!(
	firstname: 'Wojtek',
	lastname: 'Brzoza',
	email: 'brzoza@mail.com', 
	password: '12345678', 
	password_confirmation: '12345678',
)

User.create!(
	firstname: 'Kasia',
	lastname: 'Melodyjka',
	email: 'tunes@mail.com', 
	password: '12345678', 
	password_confirmation: '12345678',
)

User.create!(
	firstname: 'Zbigniewa',
	lastname: 'Zielonociotrzew',
	email: 'Zielonociotrzew@dziwnenazwiska.pl', 
	password: '12345678', 
	password_confirmation: '12345678',
)

Match.create!(
	winner_id: 1,
	loser_id: 2,
	score: 3,
	played_at: "2015-06-29 12:00:00"
)

Match.create!(
	winner_id: 1,
	loser_id: 3,
	score: 8,
	played_at: "2015-06-29 12:15:10"
)

Match.create!(
	winner_id: 1,
	loser_id: 4,
	score: 1,
	played_at: "2015-06-29 13:00:12"
)

Match.create!(
	winner_id: 2,
	loser_id: 3,
	score: 6,
	played_at: "2015-06-29 13:12:34"
)

Match.create!(
	winner_id: 3,
	loser_id: 4,
	score: 7,
	played_at: "2015-06-29 14:18:20"
)
