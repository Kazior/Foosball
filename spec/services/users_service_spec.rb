require 'rails_helper'

RSpec.describe UsersService do
	let(:subject)	{ UsersService.new }

	context "statistics" do
		let(:user1)		{ FactoryGirl.create(:user, id: 1) }
		let(:user2)		{ FactoryGirl.create(:user, id: 2) }
		let(:user) 		{ FactoryGirl.create(:user, id: 3) }

		let(:match1)	{ FactoryGirl.create(:match, winner_id: 1, loser_id: 2, score: 2) }
		let(:match2)	{ FactoryGirl.create(:match, winner_id: 1, loser_id: 2, score: 3) }
		let(:match3)	{ FactoryGirl.create(:match, winner_id: 1, loser_id: 2, score: 0) }
		let(:match4)	{ FactoryGirl.create(:match, winner_id: 2, loser_id: 1, score: 9) }		

		before do
      user1.matches << [match1, match2, match3, match4]
      user2.matches << [match1, match2, match3, match4]
    end
		
		context "#ratio" do
			it "calculates player ratio" do
				allow(subject).to receive(:win_count) { 3 }
				allow(subject).to receive(:lose_count) { 1 }
				expect(subject.ratio(user1)).to eq 0.75
			end
		end

		context "#perfect_player?" do
			it "returns true if player has any perfect matches" do
				allow(subject).to receive(:perfect_count) { 1 }
				expect(subject.perfect_player?(user)).to eq true
			end
		end

		context "#perfect_count" do
			it "counts player's perfect matches" do
				expect(subject.perfect_count(user1)).to eq 1
				expect(subject.perfect_count(user2)).to eq 0
			end
		end

		context "#win_count" do
			it "returns player's win matches" do
				expect(subject.win_count(user1)).to eq 3
				expect(subject.win_count(user2)).to eq 1
			end
		end

		context "#lose_count" do
			it "counts player's lost matches" do
				expect(subject.lose_count(user1)).to eq 1
				expect(subject.lose_count(user2)).to eq 3
			end
		end

		context "#points" do
			it "returns player's points" do
				expect(subject.points(user1)).to eq 3
				expect(subject.points(user2)).to eq -2
			end
		end
	end
end
