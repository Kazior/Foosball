require 'rails_helper'

RSpec.describe Match do
	let(:subject)				{ FactoryGirl.build(:match) }
	let(:invalid_match1) { FactoryGirl.build(:match, winner_id: 1, loser_id: 1)}
	let(:invalid_match2) { FactoryGirl.build(:match, played_at: Time.now + 2.days)}

	describe 'validation' do
		date = Time.now

		it { should validate_presence_of :winner_id }
		it { should validate_presence_of :loser_id }
		it { should validate_presence_of :score }
		it { should validate_presence_of :played_at }
		it { should allow_value(date).for(:played_at) }

	  it "does not allow played_at to be set in the future" do
	    expect(invalid_match2).to be_invalid
	  end		

	  it "does not allow winner_id and loser_id to be the same" do
	    expect(invalid_match1).to be_invalid
	  end		
	end
end
