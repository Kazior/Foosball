require 'rails_helper'

RSpec.describe User do
	let(:subject) { FactoryGirl.build(:user) }
	let(:user_admin) { FactoryGirl.build(:admin) }

	describe 'validation' do
		it { should validate_presence_of :firstname }
		it { should validate_presence_of :lastname }
		it { should validate_presence_of :email }
		it { should validate_uniqueness_of :email }
	end

	describe 'new user' do
	  it "by default is not admin" do
	    expect(subject).not_to be_admin
	  end
	end

	context '#admin' do
	  it "returns true if is admin" do
	  	expect(user_admin.admin).to be true
	  end
	end
end
