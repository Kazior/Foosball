require "rails_helper"

RSpec.describe UsersHelper do
	let(:user) { FactoryGirl.build(:user, firstname: "Arnold", lastname: "Schwarzenegger") }

  describe "#fullname" do
    it "returns correct fullname" do
			expect(helper.fullname(user)).to eq "Arnold Schwarzenegger"
    end

    it "not returns incorect fullname" do
			expect(helper.fullname(user)).not_to eq "Schwarzenegger Arnold"
    end
  end

  describe "#user_matches_balance" do
  	let(:win_count) { Faker::Number.number(2) }
  	let(:lose_count) { Faker::Number.number(2) }
  	let(:balance) { "win: #{win_count},\n\t\tlost: #{lose_count}" }

  	it "returns user matches balance" do
	  	@users_service = double
	  	
  		expect(@users_service).to receive(:win_count).with(user).and_return(win_count)
  		expect(@users_service).to receive(:lose_count).with(user).and_return(lose_count)
 
  		expect(helper.user_matches_balance(user)).to eq balance
  	end
  end
end
