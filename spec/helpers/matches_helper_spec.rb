require "rails_helper"

RSpec.describe MatchesHelper do
  let(:user) { FactoryGirl.build(:user) }
  let(:user2) { FactoryGirl.build(:user) }
  let(:match) { FactoryGirl.build(:match) }
  let(:url) { Faker::Internet.url }
  let(:avatar) { double }
  let(:size) { double }
  let(:users) { [user, user2] }
  let(:id) { Faker::Number.number(5) }
  let(:fullname) { Faker::Name.name }
  let(:score) { Faker::Number.number(1) }

  describe "#winner_avatar_url" do
    it "returns avatar url" do
      expect(helper).to receive(:winner).with(match).and_return(user)
      expect(user).to receive(:avatar).and_return(avatar)
      expect(avatar).to receive(:url).with(size).and_return(url)

      expect(helper.winner_avatar_url(match, size)).to eq url
    end
  end

  describe "#loser_avatar_url" do  
    it "returns avatar url" do
      expect(helper).to receive(:loser).with(match).and_return(user)
      expect(user).to receive(:avatar).and_return(avatar)
      expect(avatar).to receive(:url).with(size).and_return(url)

      expect(helper.loser_avatar_url(match, size)).to eq url
    end
  end

  describe "#winner" do
    it "returns winner" do
      expect(match).to receive(:users).and_return(users)
      expect(match).to receive(:winner_id).and_return(id)
      expect(users).to receive(:find).with(id).and_return(user)

      expect(helper.winner(match)).to eq user
    end
  end

  describe "#loser" do
    it "returns loser" do
      expect(match).to receive(:users).and_return(users)
      expect(match).to receive(:loser_id).and_return(id)
      expect(users).to receive(:find).with(id).and_return(user)

      expect(helper.loser(match)).to eq user
    end
  end

  describe "#result" do
    it "returns match result" do
      expect(match).to receive(:score).and_return(score)
      expect(helper.result(match)).to eq "10 : #{score}"
    end
  end

  describe "#date" do
    let(:date) { Date.today }
    let(:date_format) { "%d/%m/%Y" }
    let(:output_date) { date.strftime(date_format) }

    it "returns match date" do
      expect(match).to receive(:played_at).and_return(date)
      expect(date).to receive(:strftime).with(date_format).and_return(output_date)
      
      expect(helper.date(match)).to eq output_date
    end
  end

  describe "#time" do
    let(:time) { Time.now }
    let(:time_format) { "%H:%M" }
    let(:output_time) { time.strftime(time_format) }
    
    it "returns match time" do
      expect(match).to receive(:played_at).and_return(time)
      expect(time).to receive(:strftime).with(time_format).and_return(output_time)
      
      expect(helper.time(match)).to eq output_time
    end
  end

  describe "#winner_fullname" do
    it "returns winner fullname" do
      expect(helper).to receive(:winner).with(match).and_return(user)
      expect(helper).to receive(:fullname).with(user).and_return(fullname)

      expect(helper.winner_fullname(match)).to eq fullname
    end
  end

  describe "#loser_fullname" do
    it "returns loser fullname" do
      expect(helper).to receive(:loser).with(match).and_return(user)
      expect(helper).to receive(:fullname).with(user).and_return(fullname)

      expect(helper.loser_fullname(match)).to eq fullname
    end
  end
end
