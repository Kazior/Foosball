FactoryGirl.define do
  factory :match, class: Match do |m|
    m.winner_id { Faker::Number.number(5) }
    m.loser_id { Faker::Number.number(5) }
    m.score { Faker::Number.number(1) }
    m.surprise_victory { false }
  	m.played_at { Time.current }
  end
end
