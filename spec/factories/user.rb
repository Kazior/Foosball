FactoryGirl.define do
  factory :user do
    firstname { Faker::Name.first_name }
    lastname  { Faker::Name.last_name }
    email     { Faker::Internet.email }
    password "12345678"
    password_confirmation '12345678'
  end

  factory :admin, class: User do
    firstname "Admin"
    lastname  "User"
    admin true
  end
end
