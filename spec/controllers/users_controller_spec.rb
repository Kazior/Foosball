require 'rails_helper'

RSpec.describe UsersController do
	include Devise::TestHelpers

	let(:user) { FactoryGirl.create(:user) }
	let(:user2) { FactoryGirl.create(:user) }

  describe 'GET show' do
		it "gives profile page" do
			get :show, { id: user.id }
			expect(response).not_to redirect_to(new_user_session_path)
			expect(response.status).to eq(200)
		end
	end

  describe 'GET index' do
    it 'exposes all players' do
      get :index
      expect(controller.users).to eq([user])
      expect(response.status).to eq(200)
    end
  end
end
