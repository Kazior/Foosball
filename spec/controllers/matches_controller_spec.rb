require 'rails_helper'

RSpec.describe MatchesController do
	include Devise::TestHelpers

  let(:match) { FactoryGirl.build(:match) }

	let(:valid_attributes) { 
		{ winner_id: match.winner_id, loser_id: match.loser_id, score: match.score, played_at: match.played_at } 
	}

  let(:valid_session) { {} }

  let(:user) { FactoryGirl.build(:user) }	


	before do
    sign_in user
    expect(controller).to receive(:user_signed_in?).and_return(true)
    expect(controller).to receive(:current_user).and_return(user)
    expect(controller).to receive(:authenticate_user!).and_return(user)
  end 

	context 'user is not an admin' do
  	before do
     	expect(user).to receive(:admin).and_return(false)
    end

	  describe 'GET new' do
	    it 'redirects user to the login page' do
	      get :new, {}, valid_session
	      expect(response).to redirect_to(new_user_session_path)
	    end
	  end

	  describe 'POST create' do
	    it 'redirects user to the login page' do
	      post :create, {match: valid_attributes}, valid_session
	      expect(response).to redirect_to(new_user_session_path)
	    end
	  end
	end

	context 'user is admin' do
  	before do
     	expect(user).to receive(:admin).and_return(true)
    end

    describe 'GET new' do
      it 'exposes a new match' do
        get :new, {}, valid_session
        expect(controller.match).to be_a_new(Match)
      end
    end

    describe 'POST create' do
      before do
        expect(controller).to receive(:surprise?).and_return(false)
      end

      context 'with valid params' do        
        before do
          expect(controller).to receive(:join_match_with_users)
        end

        it 'creates a new match' do
          expect {
            post :create, {match: valid_attributes}, valid_session
          }.to change(Match, :count).by(1)
        end

        it 'exposes a newly created match as #match' do
          post :create, {match: valid_attributes}, valid_session
          expect(controller.match).to be_a(Match)
        end

        it 'redirects to the root path' do
          post :create, {match: valid_attributes}, valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'with invalid params' do
        it 'exposes a newly created but unsaved match' do
					expect_any_instance_of(Match).to receive(:save).and_return(false)
          post :create, {match: {score: 'invalid value'}}, valid_session
          expect(controller.match).to be_a_new(Match)
        end

        it "re-renders the template" do 
					expect_any_instance_of(Match).to receive(:save).and_return(false)
          post :create, {match: {score: 'invalid value'}}, valid_session
          expect(controller.match).to render_template('new')       	
        end
      end
    end
  end
end
