Rails.application.routes.draw do
  devise_for :users

  resources :matches, :users

  devise_scope :user do
    get "new_user_session", to: "devise/sessions#new" 
    get '/logout',  :to => "devise/sessions#destroy"
  end

  root 'matches#index'
end
